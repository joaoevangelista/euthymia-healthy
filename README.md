# Healthy

This project is intended to provide a view of the current Euthymia server
but it can be used by other projects.


Dependencies
---

 - Go `1.6`

Build and Run
 ---

 Set your endpoints configuration at `application.json` file following the example
 those are the endpoints our service will query, if the status of response is `200`
 that means the service is health if not the service has some problems

 Execute `go build` to build your binary then `go run main.go` to start


License
---
Made by [joaoevangelista](https://github.com/joaoevangelista)

Licensed under [MIT License]()
