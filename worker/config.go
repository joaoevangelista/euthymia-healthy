package worker

type Endpoint struct {
	Name string `required:"true"`
	Url  string `required:"true"`
}

type Config struct {
	Endpoints []Endpoint `required:"true"`
	Port      uint       `required:"true" env:"PORT" default:"8080"`
}
