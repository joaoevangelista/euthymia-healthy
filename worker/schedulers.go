package worker

import (
	"fmt"
	"net/http"

	"github.com/boltdb/bolt"
	"github.com/jasonlvhit/gocron"
)

var cfg Config
var db *bolt.DB

func InitSchedulers(config Config, database *bolt.DB) {
	cfg = config
	db = database
	registerAllEndpoints(config.Endpoints)
	gocron.Start()
}

func registerAllEndpoints(eps []Endpoint) {
	for _, ep := range eps {
		registerScheduler(ep.Url, ep.Name)
	}
}

func registerScheduler(addr string, name string) {
	fmt.Printf("Registering scheduler for %s", name)
	gocron.Every(1).Minute().Do(queryAddress, addr, name)
}

func queryAddress(addr string, name string) {
	res, err := http.Get(addr)
	if err != nil {
		fmt.Printf("Query for %s resulted in an error: %s", addr, err)
		setError(name)
	} else {
		if res.StatusCode == 200 {
			setSuccess(name)
		} else {
			setError(name)
		}
	}
}

func setError(name string) {
	db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("statuses"))
		b.Put([]byte(name), []byte("error"))
		return nil
	})
}

func setSuccess(name string) {
	db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("statuses"))
		b.Put([]byte(name), []byte("success"))
		return nil
	})
}
