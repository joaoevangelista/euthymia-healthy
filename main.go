package main

import (
	"fmt"
	"time"

	"github.com/boltdb/bolt"
	"github.com/jinzhu/configor"
	"gitlab.com/euthymia/healthy/web"
	"gitlab.com/euthymia/healthy/worker"
)

func main() {
	// Load configuration
	config := &worker.Config{}
	configor.Load(config, "application.json")
	fmt.Println("configuration loaded.")
	// Initiate Storage

	db, err := bolt.Open("healthy.db", 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		panic(err)
	}
	db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte("statuses"))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}
		return nil
	})

	// Initiate Schedulers
	worker.InitSchedulers(*config, db)

	// Start Web Server
	web.Start(*config, db)
}
