package web

import (
	"fmt"
	"net/http"

	"gitlab.com/euthymia/healthy/worker"

	"github.com/boltdb/bolt"
	"github.com/flosch/pongo2"
)

// Preload the template
var index = pongo2.Must(pongo2.FromFile("templates/index.html"))

func IndexPage(w http.ResponseWriter, r *http.Request, eps []worker.Endpoint, db *bolt.DB) {
	db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("statuses"))

		// Dynamic populate an array with values inside the database
		values := make([]Result, len(cfg.Endpoints))
		for i, e := range cfg.Endpoints {
			r := b.Get([]byte(e.Name))
			values[i] = Result{Name: e.Name, Status: string(r)}
			fmt.Printf(string(r))
		}

		err := index.ExecuteWriter(pongo2.Context{"statuses": values}, w)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return nil
	})
}

type Result struct {
	Name   string
	Status string
}
