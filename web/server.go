package web

import (
	"fmt"
	"log"
	"net/http"

	"github.com/boltdb/bolt"
	"github.com/gorilla/mux"
	"gitlab.com/euthymia/healthy/worker"
)

var db *bolt.DB
var cfg worker.Config

func Start(c worker.Config, d *bolt.DB) {
	db = d
	cfg = c

	r := mux.NewRouter()
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		IndexPage(w, r, cfg.Endpoints, db)
	}).Methods("GET")
	r.PathPrefix("/").Handler(http.FileServer(http.Dir("./static/")))
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", cfg.Port), r))
}
